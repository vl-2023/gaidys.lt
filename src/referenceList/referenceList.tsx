import React from "react"
import "./referenceList.css"

type Props = {
  children: React.ReactNode
}

const ReferenceList = ({children }: Props) => {
  return (
    <span
      className="referenceList"
    >
     {children}
    </span>
  )
}

export default ReferenceList
