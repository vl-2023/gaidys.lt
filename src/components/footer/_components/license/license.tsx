import "./style.css";
import { igemDarkBlue } from "../../../../importStatic/staticStyles";

const igemDarkBlueStyle:React.CSSProperties = {
  backgroundColor: igemDarkBlue
}

const License = () => (
  <div className={" w-full t-center bg-igem-footer"} style= {igemDarkBlueStyle}>
    <span className="font-raleway-light font-light t-xs">
      Ⓒ 2023 - Content on this site is licensed under a{" "}
    </span>
    <a
      href="https://creativecommons.org/licenses/by/4.0/"
      target="_blank"
      className="font-raleway-medium font-semibold t-xs"
    >
      Creative Commons Attribution 4.0 International license
    </a>
    <span className="font-raleway-light font-light t-xs">
      . The repository used to create this website is available at
    </span>
    <a
      className="t-igem-blue t-xs font-raleway-light font-light"
      href="https://gitlab.igem.org/2023/vilnius-lithuania"
      target="_blank"
    >
      {" "}
      gitlab.igem.org/2023/vilnius-lithuania.
    </a>
  </div>
);

export default License;
