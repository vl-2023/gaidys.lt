import "./mobileFooter.css";
import {
   mockpractica,
   realLogo,
   mockWfacebook,
   mockthermaks,
   mockgmc
  } from "../../../../importStatic/mockData/images";

const MobileFooter = () => (
  <div className="footerMobileContainer">
    <div className="logoSocialContainer">
      <div className="logoContainer">
            <img className= "imageLogo" src={realLogo}></img>
      </div>
      <div className="socialContainer">
          <div className="socialContainer1">
            <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
            <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
            <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
          </div>
          <div className="socialContainer2">
          <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
          <a className="displayButtonONLY" href="https://www.facebook.com/VilniusiGEM?locale=lt_LT" target="_blank"><button className="imageSocialButton"><img className="imageSocial" src={mockWfacebook}></img></button></a>
          </div>  
        </div>
    </div>

    <div className="sponsorContainerMobile">
      <div className="sponsorBigMobile">
      <img className = "imageSponsorBMobile"alt="Error404" src={mockgmc}></img>
      <img className = "imageThermofisherMobile"alt="Error404" src={mockthermaks}></img>
      <img className = "imageSponsorBMobile"alt="Error404" src={mockgmc}></img>
      </div>        
      <div className="sponsorSmallMobile">
        <img className = "imageSponsorSMobile"alt="Error404" src={mockpractica}></img>
        <img className = "imageSponsorSMobile"alt="Error404" src={mockpractica}></img>
        <img className = "imageSponsorSMobile"alt="Error404" src={mockpractica}></img>
        <img className = "imageSponsorSMobile"alt="Error404" src={mockpractica}></img>
        <img className = "imageSponsorSMobile"alt="Error404" src={mockpractica}></img>
      </div>
    </div>
  </div>
  )
;
export default MobileFooter;
