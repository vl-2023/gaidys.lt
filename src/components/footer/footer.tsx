import License from "./_components/license/license";
import DesktopFooter from "./_components/desktopFooter/desktopFooter";
import MobileFooter from "./_components/mobileFooter/mobileFooter";
import useWindowDimensions from "../../customHooks/useWindowDimensions";
import { actualMdWidth } from "../../importStatic/sizeConsts";

const Footer = () => {
  const { width } = useWindowDimensions();
  return (
    <div>
      {width && width >= actualMdWidth ? <DesktopFooter /> : <MobileFooter/>}
      <License />
    </div>
  );
};
export default Footer;
