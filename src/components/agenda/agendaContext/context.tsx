import React from "react"

export const AgendaContext = React.createContext<boolean>(false)

type Props = {
  children: React.ReactNode
  isWithAgenda: boolean
}

const AgendaContextProvider = ({ children, isWithAgenda }: Props) => {
  return (
    <AgendaContext.Provider value={isWithAgenda}>
      {children}
    </AgendaContext.Provider>
  )
}

const useAgendaContext = () => {
  const context = React.useContext(AgendaContext)

  return context
}

export { AgendaContextProvider, useAgendaContext }
