import React, { CSSProperties } from "react"

type Props = {
  children: React.ReactNode
}
const HeadingStyle: CSSProperties = {
        marginLeft: 0.25,
        fontWeight: 300,
        color: "ffffff", 
        cursor: "pointer" 
      }; 
const HeadingText = ({ children}: Props) => {
  return (
    <span style={HeadingStyle}>
      {children}
    </span>
  )
}

export default HeadingText
