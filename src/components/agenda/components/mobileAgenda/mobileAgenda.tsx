import React from "react"
import OutsideAlerter from "../../../OutsideAlerter/OutsideAlerter"
import { MobileAgendaProps } from "../../types/agendaTypes"
//import "../DesktopAgenda/styles.css"

export default function MobileAgenda({
  activeId,
  headings,
  onScroll,
  title,
  listHeight = "h-[280px]",
}: MobileAgendaProps) {
  const [isOpen, setIsOpen] = React.useState(false)

  return (
    <OutsideAlerter onClick={() => setIsOpen(false)}>
      <nav
        className={`flex flex-col items-center fixed bottom-0 z-10 w-full box-border transform transition-all duration-500 bg-igem-light-blue`}
      >
        <div
          className="block w-full h-8 text-center py-2 box-border cursor-pointer z-10"
          onClick={() => setIsOpen(!isOpen)}
        >
          {isOpen ? (
            <img
              alt=""
              className="transition-all h-full left-1/2 relative"
              src={"closeIcon"}
            />
          ) : (
            <span className="uppercase text-base font-medium font-jost-medium text-center text-white">
              Index
            </span>
          )}
        </div>
        <div
          className={`max-h-[70vh] flex flex-col w-full transition-all duration 500 ${
            isOpen ? listHeight : "h-0"
          }`}
        >
          <span className="font-jost-medium font-medium py-2 text-2xl text-white text-center">
            {title}
          </span>
          <ul className={`w-full pb-4`}>
            {headings.map(heading => (
              <li
                key={heading.id}
                className="flex align-middle py-1 px-16 sm:px-32"
                onClick={_e => onScroll(heading.id)}
                style={{
                  marginLeft: `${heading.level - 1}em`,
                }}
              >
                <img
                  alt=""
                  className={`scale-75 ${
                    activeId === heading.id ? "visible text-white" : "invisible"
                  }`}
                  src={"headingTriangleWhite"}
                />
              </li>
            ))}
          </ul>
        </div>
      </nav>
    </OutsideAlerter>
  )
}
