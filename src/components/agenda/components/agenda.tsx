import React from "react"
import useWindowDimensions from "../../../customHooks/useWindowDimensions"
import { mdWidth } from "../../../importStatic/sizeConsts"
import useHeadings from "../hooks/useHeading"
import useScrollSpy from "../hooks/useScroll"
import DesktopAgenda from "./desktopAgenda/desktopAgenda"
import MobileAgenda from "./mobileAgenda/mobileAgenda"

type Props = {
  title: string
  mobileHeight?: string
  isShown?: boolean
}

function Agenda({ title, isShown, mobileHeight}: Props) {
  const headings = useHeadings()
  const { width } = useWindowDimensions()
  const activeId = useScrollSpy(
    headings.map(({ id }) => id),
    { rootMargin: "0% 0% 0% 0%" }
  )

  const handleScroll = (id: string) => {
    document
      .getElementById(id)
      ?.scrollIntoView({ block: "center", behavior: "smooth" })
  }

  if (width && width < mdWidth) {
    return (
      <MobileAgenda
        activeId={activeId}
        headings={headings}
        onScroll={handleScroll}
        title={title}
        listHeight={mobileHeight}
      />
    )
  }

  return (
    <DesktopAgenda
      isShown={isShown}
      activeId={activeId}
      headings={headings}
      onScroll={handleScroll}
      title={title}
    />
  )
}

export default Agenda
