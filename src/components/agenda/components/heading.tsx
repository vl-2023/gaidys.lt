import React from "react"

type HeadingProps = {
  children: React.ReactNode
  as: "h1" | "h2" | "h3" | "h4" | "h5" | "h6"
  id: string
}

function Heading({ children, as: Element, id }: HeadingProps) {
  return <Element id={id}>{children}</Element>
}

export default Heading
