import React from "react"
import useSeeAgenda from "../../customHooks/useSeeAgenda"
import useWindowDimensions from "../../customHooks/useWindowDimensions"
import getFooterHeight from "../../customHooks/getFooterHeight"
import { Agenda, AgendaContextProvider } from "../agenda"
import Footer from "../footer/footer"
//import MainHeader from "../MainHeader"
//import { MainHeaders } from "../MainHeader/types/mainHeaders"
import Navigation from "../navbar"
import ProgressBar from "../../components/progressBar/index/progressBar"
import ShittyHeader from "../header/header"
import { mockbetterPNG } from "../../importStatic/mockData/images"


type Props = {
  agendaTitle?: string
  children: React.ReactNode
  //isWithBackToTop?: boolean
  isWithAgenda?: boolean
  isWithProgressBar?: boolean
  isWithBackground?: boolean
  //header: MainHeaders
  agendaHeight?: string
  //isTurtleSmart?: boolean
  isPageLong?: boolean
}

const Layout = ({
  agendaTitle,
  children,
  //isWithBackToTop = false,
  isWithAgenda = false,
  isWithProgressBar = false,
  isWithBackground = true,
  //header,
  agendaHeight,
  //isTurtleSmart = false,
  isPageLong,
}: Props) => {
  const headerList = ["Description", "Communication"]
  const { width } = useWindowDimensions()
  const currentUrl = window.location.href;
  const headerType = currentUrl.split('/');
  const avoidCrash = headerType[4].split('#')
  let headerComponent = null;
  if (headerList.includes(avoidCrash[0])) {
    const finalHeader = avoidCrash[0].toUpperCase();
    headerComponent = (
      <ShittyHeader backgroundImageUrl={mockbetterPNG} headerText={finalHeader} />
    );
  }
  const footerHeight = getFooterHeight(width || 0)
  const isShown = useSeeAgenda(footerHeight)
  const renderChildren = () => (
    <AgendaContextProvider isWithAgenda={isWithAgenda}>
      <>{children}</>
    </AgendaContextProvider>
  )

  return (
    <>
      <Navigation />
      {headerComponent}
        {renderChildren()}
        {isWithAgenda && (
          <Agenda
            title={agendaTitle || "Agenda title"}
            isShown={isShown}
            mobileHeight={agendaHeight}
          />
        )}
        {isWithProgressBar && (
          <ProgressBar isShown={isShown} footerHeight={footerHeight} />
        )}
      <Footer />
    </>
  )
}

export default Layout
