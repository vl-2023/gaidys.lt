import React from "react"
import "./style.css"

type Props = {
  children: React.ReactNode
  className?: string
  id?: string
}

const Paragraph = ({ children, className, id }: Props) => {
  return (
    <p
      id={id}
      className={
        `paragraph ` +
        className
      }
    >
      {children}
    </p>
  )
}

export default Paragraph
