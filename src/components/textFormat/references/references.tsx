import React from "react"
import "./reference.css"

type Props = {
    children: React.ReactNode
}

const refrences = ({children}:Props) => {
    return(
        <span className="reference"><a>{children}</a></span>
    )
}

export default refrences

