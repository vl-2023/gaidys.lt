import React from "react"
import { ITextProps } from "../text/textTypes"
import "./header.css"
interface Props extends ITextProps {}

const Header = (props: Props) => {
  const className = `header`
  return <span className={className}>{props.children}</span>
}

export default Header
