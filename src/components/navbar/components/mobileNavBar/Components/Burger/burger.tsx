import { mockSVGURL } from "../../../../../../importStatic/mockData/images"
import { instaSVG } from "../../../../../../importStatic/mockData/images" 
import "./burger.css"

type Props = {
  isOpen: boolean
  onClick: () => void
}

const Burger = ({ isOpen, onClick }: Props) => {
  return (
    <img
      alt=""
      className={`pointer ${isOpen ? "closed" : "open"}`}
      src={isOpen ? mockSVGURL : instaSVG}
      onClick={() => onClick()}
    />
  )
}

export default Burger
