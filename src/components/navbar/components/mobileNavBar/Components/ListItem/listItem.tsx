import React from "react"
import { CSSProperties } from "react";
import { PageSections } from "../../../desktopNavBar/Components/types/links"
import ListItemContent from "./components/listItemContentM/listItemContentM"
import SubList from "./components/subList/subList"

type Props = {
  children: React.ReactNode
  section: PageSections
  onClick: () => void
  isOpen: boolean
}
const cursorStyle: CSSProperties = {
  cursor: "pointer"
};    
const ListItem = ({ children, section, onClick, isOpen }: Props) => {
  return (
    <>
      <li style={cursorStyle} onClick={onClick}>
        <ListItemContent isOpen={isOpen}>{children}</ListItemContent>
      </li>
      <SubList isOpen={isOpen} section={section} />
    </>
  )
}

export default ListItem
