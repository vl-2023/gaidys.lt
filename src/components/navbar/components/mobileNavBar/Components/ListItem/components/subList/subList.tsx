import { useNavigate } from "react-router-dom"
import { PageRoutesMap, PageSections } from "../../../../../desktopNavBar/Components/types/links"
import "./subList.css"


type Props = {
  isOpen: boolean
  section: PageSections
}

const SubList = ({ isOpen, section }: Props) => {
    const navigate = useNavigate();
  return (
    <ul
      className={`subListM ${
        isOpen ? `openL` : "closedL"
      }`}
    >
      {PageRoutesMap[section].map((item, i) => (
        <li
          key={i}
          className="subListList"
          onClick={() => {
            navigate(`/vilnius-lithuania/${item.route}`)
          }}
        >
          {item.label}
        </li>
      ))}
    </ul>
  )
}

export default SubList
