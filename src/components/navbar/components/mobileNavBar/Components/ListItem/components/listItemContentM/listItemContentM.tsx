import React from "react"
import "./listItemContext.css"
import { mockSVGURL } from "../../../../../../../../importStatic/mockData/images"

type Props = {
  children: React.ReactNode
  isOpen: boolean
}

const ListItemContent = ({ children, isOpen }: Props) => (
  <div className="ListTheList">
    <div>
      <span className="ListSpan">
        {children}
      </span>
    </div>
    <div>
      <img
        alt="arrow"
        className={`wheel ${isOpen ? "spin" : ""}`}
        src={mockSVGURL}
      />
    </div>
  </div>
)

export default ListItemContent
