import React from "react"
import Burger from "./Components/Burger/burger"
import { listItems } from "../desktopNavBar/Components/types/ListItems"
import ListItem from "./Components/ListItem/listItem"
import { logoexullose } from "../../../../importStatic/mockData/images"
import { useNavigate } from "react-router-dom"
import { PageRoutesMap } from "../desktopNavBar/Components/types/links"
import "./mobilenavbar.css"

const MobileNavBar = () => {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = React.useState(false)
  const [isSubListsOpen, setIsSubListsOpen] = React.useState<boolean[]>(
    Object.keys(PageRoutesMap).map(_ => false)
  )

  const handleSubListOpen = (index: number) => {
    const newArr = isSubListsOpen.map((item, i) => (i === index ? !item : item))
    setIsSubListsOpen([...newArr])
  }

    
  

    return(
      <>
      <div className="mainContainer">
        <Burger isOpen={isOpen} onClick={() => setIsOpen(!isOpen)} />
        <img className="logoSmall" onClick={()=>navigate("/vilnius-lithuania/")} src={logoexullose} alt="burger" />
      </div>
      <div
        className={`burgerListContainer ${
          isOpen ? "" : "active"
        }`}
      >
        <ul className="burgerList">
          {listItems.map((item, i) => (
            <ListItem
              key={i}
              section={item.section}
              onClick={() => handleSubListOpen(i)}
              isOpen={isSubListsOpen[i]}
            >
              {item.label}
            </ListItem>
          ))}
        </ul>
      </div>
    </>
    )

}
export default MobileNavBar

