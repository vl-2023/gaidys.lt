import { PageSections } from "./links"

export type ListItemData = {
  section: PageSections
  label: string
}

export const listItems: ListItemData[] = [
  { section: PageSections.Project, label: "Project" },
  { section: PageSections.WetLab, label: "Wet Lab" },
  { section: PageSections.DryLab, label: "Dry Lab" },
  { section: PageSections.HP, label: "Human Practices" },
  { section: PageSections.People, label: "People" },
]

