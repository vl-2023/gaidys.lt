export enum PageSections {
    Project = "project",
    WetLab = "wetLab",
    DryLab = "dryLab",
    HP = "hp",
    People = "people",
  }
  
  export type PageRoute = {
    route: string
    label: string
  }
  
  export type PageRoutesMapData = {
    [key in PageSections]: PageRoute[]
  }
  
  export const PageRoutesMap: PageRoutesMapData = {
    [PageSections.Project]: [
      { label: "Description", route: "description" },
      { label: "Contribution", route: "contribution" },
      { label: "Implementation", route: "implementation" },
      { label: "Safety", route: "safety" },
      { label: "Judging", route: "judging" },
    ],
    [PageSections.WetLab]: [
      { label: "Engineering", route: "engineering" },
      { label: "Proof of Concept", route: "proof-of-concept" },
      { label: "Experiments", route: "experiments" },
      { label: "Design", route: "design" },
      { label: "Results", route: "results" },
      { label: "Part Collection", route: "part-collection" },
      { label: "Materials", route: "materials" },
    ],
    [PageSections.DryLab]: [
      { label: "Model", route: "model" },
      { label: "Software", route: "software" },
    ],
    [PageSections.HP]: [
      { label: "Integrated Human Practices", route: "human-practices" },
      { label: "Scientific communication", route: "communication" },
      { label: "Education", route: "education" },
      { label: "Inclusivity", route: "inclusivity" },
      { label: "MENASXMOKSLAS", route: "menasxmokslas" },
    ],
    [PageSections.People]: [
      { label: "Team", route: "team" },
      { label: "Attributions", route: "attributions" },
    ],
  }
  