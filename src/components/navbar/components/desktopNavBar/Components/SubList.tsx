import React from "react";
import { PageRoutesMap, PageSections } from "./types/links";
import SubListItem from "./sublistItem";
import "./SubList.css";

type Props = {
  isLast: boolean;
  isOpen: boolean;
  section: PageSections;
};

const SubList = ({ isLast, isOpen, section }: Props) => (
  <div
    className={`subListContainer ${isOpen ? "visible" : ""} ${
      isLast ? "right-2" : ""
    }`}
  >
    <ul className="subList">
      {PageRoutesMap[section].map((s, i, arr) => (
        <SubListItem
          isFirst={i === 0}
          isLast={i === arr.length - 1}
          isOpen={isOpen}
          key={s.route}
          subItem={s}
        />
      ))}
    </ul>
  </div>
);

export default SubList;