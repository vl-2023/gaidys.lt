import React from "react"
import { ListItemData } from "./types/ListItems"
import ListItem from "./ListItem"
import "./List.css"

type Props = {
  listItems: ListItemData[]
}

const List = ({ listItems }: Props) => (
  <ul className="listItems listItem1">
    {listItems.map((item, i, arr) => (
      <ListItem isLast={i === arr.length - 1} key={i} section={item.section}>
        {item.label}
      </ListItem>
    ))}
  </ul>
)

export default List



