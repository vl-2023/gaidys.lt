import React from "react"
import "./style.css"

type Props = {
  children: React.ReactNode
  isDisabled?: boolean
  onClick: () => void
}

const Button = ({
  children,
  isDisabled = false,
  onClick,
}: Props) => {
  return (
    <button
      className="button"
      disabled={isDisabled}
      onClick={onClick}
      role="button"
      tabIndex={0}
    >
      {children}
    </button>
  )
}

export default Button
