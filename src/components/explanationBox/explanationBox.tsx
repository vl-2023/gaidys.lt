import React from "react"
import "./explanationBox.css";

type Props = {
    children: string;
    explanation: string;
};

const explanationBox = ({children, explanation}: Props) => {

  return (
    <div>
        <div className="explanationBoxBox">
            {explanation}
        </div>
        <div className="explanationBoxText">
            {children}
        </div>
    </div>
  )
}

export default explanationBox