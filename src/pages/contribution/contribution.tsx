import Footer from "../../components/footer/footer"
import Paragraph from "../../components/paragraph/paragraph"
import NavBar from "../../components/navbar"
import { loremMultiParagaph } from "../../importStatic/mockData/lorem"
import Article from "../../components/article/article"
import Texts from "../../components/textFormat/text/text"
import { Heading } from "../../components/agenda"
import Header from "../../components/header/header"
import { mockbetterPNG } from "../../importStatic/mockData/images"

const contributionPage = () => {
    return(
        <>
        <NavBar/>
        <Header backgroundImageUrl={mockbetterPNG} headerText="CONTRIBUTION"/>
        <Article>
            <Texts.Header>
                <Heading as = "h1" id="intro">
                    Introduction
                </Heading>
            </Texts.Header>
            <Paragraph>{loremMultiParagaph}</Paragraph>
            <Texts.Header>
                The Idea
            </Texts.Header>
            <Paragraph>{loremMultiParagaph}</Paragraph>
        </Article>
        <Footer/>
        </>
    )
}
export default contributionPage