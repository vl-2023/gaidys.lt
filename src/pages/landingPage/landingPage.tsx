import NavBar from "../../components/navbar"
import { SVGline } from "../../importStatic/images/landing page/landingIMG"
import Footer from "../../components/footer/footer"

const LandingPage = () => {
    return(
        <>
        <NavBar/>
        <div>
            <img src={SVGline} alt="linija" />
        </div>
        <Footer/>
        </>
    )
}
export default LandingPage