import Article from "../../components/article/article"
import Layout from "../../components/layout/layout"
import Paragraph from "../../components/paragraph/paragraph"
import { Heading } from "../../components/agenda"
import Texts from "../../components/textFormat/text/text"
import { loremMultiParagaph } from "../../importStatic/mockData/lorem"
import Header from "../../components/header/header"
import { mockbetterPNG } from "../../importStatic/mockData/images"

const designPage = () => {
    return(
        <>
        <Layout
            isWithAgenda 
            agendaTitle="Design"
            isWithProgressBar
            agendaHeight="h-[70vh]"
              >
            <Header backgroundImageUrl={mockbetterPNG} headerText="DESIGN"/>
            <Article>
                <Paragraph>
                    <Texts.Header>
                        <Heading as= "h1" id="intro" >
                            Introduction
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                        {loremMultiParagaph}
                    </Paragraph>
                    
                    <Texts.Header>
                        <Heading  as = "h1" id="lang">
                            Language
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                        {loremMultiParagaph}
                    </Paragraph>

                    <Texts.Header>
                        <Heading  as = "h1" id="end">
                            The end
                        </Heading>
                    </Texts.Header>
                    <Paragraph>
                        {loremMultiParagaph}
                    </Paragraph>
                </Paragraph>
            </Article>
        </Layout>
        </>
    )
}
export default designPage