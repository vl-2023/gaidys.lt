import ShittyHeader from "../../components/header/header"
import NavBar from "../../components/navbar"
import { mockbetterPNG } from "../../importStatic/mockData/images"
import { loremMultiParagaph } from "../../importStatic/mockData/lorem"
import Article from "../../components/article/article"
import Paragraph from "../../components/paragraph/paragraph"
import { Heading } from "../../components/agenda"
import Texts from "../../components/textFormat/text/text"
import Footer from "../../components/footer/footer"
function softwarePage() {
    return (
        <>
        <NavBar/>
        <ShittyHeader backgroundImageUrl={mockbetterPNG} headerText="SOFTWARE"/>
        <Article>
            <Texts.Header>
                <Heading as = "h1" id="intro">
                    Introduction
                </Heading>
            </Texts.Header>
            <Paragraph>{loremMultiParagaph}</Paragraph>
            <Texts.Header>
                The Idea
            </Texts.Header>
            <Paragraph>{loremMultiParagaph}</Paragraph>
        </Article>
        <Footer/>
        </>
    )
  }
  
  export default softwarePage