import { Route, Routes } from "react-router-dom"
import AttributionsPage from "./pages/attributions/attributions"
import CommunicationPage from "./pages/communication/communication"
import ContributionPage from "./pages/contribution/contribution"
import LandingPage from "./pages/landingPage/landingPage"
import DescriptionPage from "./pages/description/description"
import SoftwarePage from "./pages/software/software"
import DesignPage from "./pages/design/design"
import EducationPage from "./pages/education/education"

function App() {
  return (
    <Routes>
      <Route path="/vilnius-lithuania/" element={<LandingPage/>}/>
      <Route path="/vilnius-lithuania/attributions" element={<AttributionsPage/>}/>
      <Route path="/vilnius-lithuania/communication" element={<CommunicationPage/>}/>
      <Route path="/vilnius-lithuania/contribution" element={<ContributionPage/>}/>
      <Route path="/vilnius-lithuania/description" element={<DescriptionPage/>}/>
      <Route path="/vilnius-lithuania/design" element={<DesignPage/>}/>
      <Route path="/vilnius-lithuania/education" element={<EducationPage/>}/>
      <Route path="/vilnius-lithuania/engineering" element=""/>
      <Route path="/vilnius-lithuania/experiments" element=""/>
      <Route path="/vilnius-lithuania/human-practices" element=""/>
      <Route path="/vilnius-lithuania/implementation" element=""/>
      <Route path="/vilnius-lithuania/model" element=""/>
      <Route path="/vilnius-lithuania/part-collection" element=""/>
      <Route path="/vilnius-lithuania/proof-of-concept" element=""/>
      <Route path="/vilnius-lithuania/results" element=""/>
      <Route path="/vilnius-lithuania/safety" element=""/>
      <Route path="/vilnius-lithuania/software" element={<SoftwarePage/>}/>
      <Route path="/vilnius-lithuania/team" element=""/>
      <Route path="/vilnius-lithuania/judging" element=""/>
      <Route path="/vilnius-lithuania/materials" element=""/>
      <Route path="/vilnius-lithuania/inclusivity" element=""/>
      <Route path="/vilnius-lithuania/menasxmokslas" element=""/>
    </Routes>
  )
}

export default App
