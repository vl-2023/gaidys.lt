import {
    actualMdWidth,
    smWidth,
    twoXLWidth,
    xlWidth,
  } from "../importStatic/sizeConsts"
  
  const getFooterHeight = (width: number) => {
    if (width > twoXLWidth) {
      return 464
    }
    if (width > xlWidth) {
      return 424
    }
    if (width > actualMdWidth) {
      return 446
    }
    if (width > smWidth) {
      return 704
    }
    return 610
  }
  
  export default getFooterHeight
  