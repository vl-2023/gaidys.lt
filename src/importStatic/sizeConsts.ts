export const smWidth = 640

export const mdWidth = 885

export const actualMdWidth = 768

export const lgWidth = 1024

export const xlWidth = 1280

export const twoXLWidth = 1536