export const mockPNGURL="https://static.igem.wiki/teams/4719/wiki/mockimages/blob.png"
export const mockSVGURL="https://static.igem.wiki/teams/4719/wiki/mockimages/rutuliukas.svg"
export const mockGIFURL="https://static.igem.wiki/teams/4719/wiki/mockimages/gif.gif"
export const mockpractica = "https://static.igem.wiki/teams/4719/wiki/sponsor/practica-capital.png"
export const mockthermaks = "https://static.igem.wiki/teams/4719/wiki/sponsor/thermakas.png"
export const mockgmc = "https://static.igem.wiki/teams/4719/wiki/sponsor/vu-gmc.png"
export const mocklogo = "https://static.igem.wiki/teams/4719/wiki/sponsor/igem-logo.png"
export const mockfacebook = "https://static.igem.wiki/teams/4719/wiki/sponsor/facebook-logo.png"
export const mockWfacebook = "https://static.igem.wiki/teams/4719/wiki/sponsor/facbooko-baltas.jpg"
export const logoexullose = "https://static.igem.wiki/teams/4719/wiki/sponsor/exullose-baltas-logo.png"
export const realLogo = "https://static.igem.wiki/teams/4719/wiki/sponsor/351509903-625744629483896-8578786035626647168-n-1.png"
export const meskHeader = "https://static.igem.wiki/teams/4719/wiki/mockimages/mesky.png"
export const mockbetterPNG = "https://static.igem.wiki/teams/4719/wiki/mockimages/betterpng.png"
export const instaSVG = "https://static.igem.wiki/teams/4719/wiki/social/insta.svg"